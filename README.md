# exercise-powershell


## how to use

### intractive mode
- pwshと打つだけ
```
root@88f7454761d7:/powershell# pwsh
PowerShell 7.3.6
PS /powershell> $PSVersionTable

Name                           Value
----                           -----
PSVersion                      7.3.6
PSEdition                      Core
### 省略 ###``P
```

### ファイル形式(.ps1)での実行
- pwshをつけて実行するだけ。
```
root@88f7454761d7:/powershell# pwsh sample.ps1

Name                           Value
----                           -----
PSVersion                      7.3.6
PSEdition                      Core
### 省略 ###
```


## osはubuntu
```
root@88f7454761d7:/powershell# cat /etc/os-release 
PRETTY_NAME="Ubuntu 22.04.2 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04.2 LTS (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy```