for ($i = 0; $i -lt 100; $i++) {
    if (($i % 3 -eq 0) -and ($i % 5 -eq 0)) {
        Write-Host "$i : FIZZBUZZ"

    }
    elseif ($i % 3 -eq 0) {
        Write-Host "$i : FIZZ"
    }
    elseif ($i % 5 -eq 0) {
        Write-Host  "$i : BUZZ"
    }
    else {
        Write-Host "$i :"
    }
}
