1..100 | ForEach-Object {
    if ((${_} % 3 -eq 0) -and (${_} % 5 -eq 0)) {
        Write-Host "${_} : FIZZBUZZ"
    }
    elseif (${_} % 3 -eq 0) {
        Write-Host "${_} : FIZZ"
    }
    elseif (${_} % 5 -eq 0) {
        Write-Host  "${_} : BUZZ"
    }
    else {
        Write-Host "${_} :"
    }
}
