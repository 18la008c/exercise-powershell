# refs: https://qiita.com/SilkyFowl/items/49d175a495c5c43a15b9
# $null + $nullは$null
1..100 | ForEach-Object {
    ($_ % 3 ? $null :"Fizz") + ($_ % 5 ? $null :"Buzz") ?? $_
}