# -matchは1つしか返却しない。
$s = "郵便番号:001-0001, 電話番号(自宅):0439-00-1234, 電話番号(携帯):080-0001-1234, 電話番号(携帯2):090-1234-5678"

$s -match "\d{3}-\d{4}-\d{4}"
Write-Output $Matches

# PS /powershell> pwsh ./sample3.ps1
# True

# Name                           Value
# ----                           -----
# 0                              080-0001-1234